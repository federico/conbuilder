# conbuilder - unit tests
#
# Copyright 2017 Federico Ceratto <federico@debian.org>
# Released under GPLv3 License, see LICENSE file

import conbuilder as cb


def test_parse_build_deps():
    out = """5 upgraded, 340 newly installed, 0 to remove and 14 not upgraded.
Inst systemd [241-7] (242-4 Debian:unstable [amd64]) []
Inst libsystemd0 [241-7] (242-4 Debian:unstable [amd64])
Conf libsystemd0 (242-4 Debian:unstable [amd64])
Inst gcc-9-base [9.2.1-2] (9.2.1-4 Debian:unstable [amd64]) [libgcc1:amd64 libstdc++6:amd64 ]
Conf gcc-9-base (9.2.1-4 Debian:unstable [amd64]) [libgcc1:amd64 libstdc++6:amd64 ]
Inst libgcc1 [1:9.2.1-2] (1:9.2.1-4 Debian:unstable [amd64]) [libstdc++6:amd64 ]
Conf libgcc1 (1:9.2.1-4 Debian:unstable [amd64]) [libstdc++6:amd64 ]
Inst libstdc++6 [9.2.1-2] (9.2.1-4 Debian:unstable [amd64])
Inst libuchardet0 (0.0.6-3 Debian:unstable [amd64])
"""
    deps = cb._parse_build_deps(out.splitlines())
    assert deps == [
        ("gcc-9-base", "9.2.1-4"),
        ("libgcc1", "1:9.2.1-4"),
        ("libstdc++6", "9.2.1-4"),
        ("libsystemd0", "242-4"),
        ("libuchardet0", "0.0.6-3"),
        ("systemd", "242-4"),
    ]
